﻿using UnityEngine;

public class RandomMovementSpeed : MonoBehaviour {
	public Vector3 direction;
	public float speedMin, speedMax;

	float speed;

	void Awake() {
		speed = Random.Range(speedMin, speedMax);
	}

	void FixedUpdate() {
		transform.Translate(direction * speed);
	}
}