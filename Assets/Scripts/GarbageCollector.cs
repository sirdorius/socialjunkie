﻿using UnityEngine;

public class GarbageCollector : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D coll) {
		Destroy(coll.transform.parent.parent.gameObject);
	}
}