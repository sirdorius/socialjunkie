﻿using UnityEngine;

public class ColoredSprite : MonoBehaviour {
	public Color color;

	void Start() {
		foreach (SpriteRenderer s in GetComponentsInChildren<SpriteRenderer>()) {
			s.color = color;
		}
	}
}