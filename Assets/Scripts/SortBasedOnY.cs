﻿using UnityEngine;

public class SortBasedOnY : MonoBehaviour {
	SpriteRenderer spriteRenderer;

	void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void FixedUpdate() {
		spriteRenderer.sortingOrder = -(Mathf.FloorToInt(transform.position.y*100));
	}
}