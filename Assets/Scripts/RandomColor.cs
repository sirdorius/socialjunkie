﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class RandomColor : MonoBehaviour {
	public List<Color> colors;

	void Awake() {
		var color = colors[Random.Range(0, colors.Count)];
		if (GetComponent<SpriteRenderer>())
			GetComponent<SpriteRenderer>().color = color;
		if (GetComponent<Image>())
			GetComponent<Image>().color = color;
	}
}