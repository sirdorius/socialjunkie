﻿using UnityEngine;

public class RandomizeSize : MonoBehaviour {
	public float xJitter, yJitter;

	void Awake() {
		Vector3 scale = transform.localScale;
		scale.x *= 1 + Random.Range(-xJitter, xJitter);
		scale.y *= 1 + Random.Range(-yJitter, yJitter);
		transform.localScale = scale;
	}
}