﻿using UnityEngine;
using DG.Tweening;

public class CameraFollow : MonoBehaviour {
	public Transform target;
	Vector3 initOffset;

	public float followSpeed = 0.15f;

	void Awake() {
		initOffset = transform.position - target.position;
	}

	void LateUpdate() {
		var pos = target.position;
		pos.x = target.position.x + initOffset.x;
		pos.y = transform.position.y;
		pos.z = transform.position.z;
		transform.DOMove(pos, followSpeed);
	}
}