﻿using UnityEngine;
using UnityEngine.UI;

public class RandomText : MonoBehaviour {
	public Text nameText, statusText;

	static string[] verbs = {"looks like", "passionately loves", "likes", "dances with", "is better than", "eats", "perplexes", "sets on fire", "has sex with", "conceals"};
	static string[] subjects = {"a french fry", "my dog", "a hungry armadillo", "a one eyed cat", "an old person", "MechaHitler", "a small boob", "a 30 minute orgasm", "one catapult", "Justin Bieber", "an old Kamikaze pilot", "crystal meth", "natural selection", "my penis", "the clitoris"};
	static string[] complements = { "potatoes", "dogs", "cement", "hemorroids", "cats", "fat", "gargoyles", "hogwash", "seppukku", "an oversized lollipop", "stray pubes", "pictures of boobs", "earwax and semen", "the morbidly obese", "poor people", "penis"};
	static string[] conjunctions = { "because", "even though", "since", "although", "if only", "unless", "whenever" };
	static string[] swearWords = { "fucking", "motherfucking", "cocksucking" };
	static string[][] phrase = { subjects, verbs, complements, conjunctions, subjects, verbs, complements };
	const float swearProbability = 0.03f;

	static string[] names = { "John", "Tim", "Rob", "Jaina", "Melissa", "Kara"};
	static string[] surnames = { "Blake", "Audley", "Corey" };


	public void Start() {
		string status = "";
		int i=0;
		foreach (var words in phrase) {
			var word = randomString(words);
			if (i == 0)
				word = word[0].ToString().ToUpper() + word.Substring(1);
			else
				if (Random.value < swearProbability)
					status += randomString(swearWords) + " ";
			status += word + " ";
			i++;
		}
		statusText.text = status;

		nameText.text = randomString(names) + " " + randomString(surnames);
	}

	public string randomString(string[] strings) {
		return strings[Random.Range(0, strings.Length)];
	}
}