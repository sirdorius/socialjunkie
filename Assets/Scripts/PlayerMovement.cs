﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public Vector2 acceleration;

	float xSpeed;
	float ySpeed = 0;

	void Awake() {
		xSpeed = acceleration.x;
	}

	void Update() {
		ySpeed = Input.GetAxis("Vertical") * acceleration.y;
	}


	void FixedUpdate() {
		GetComponent<Rigidbody2D>().AddForce(new Vector3(xSpeed, ySpeed));
		xSpeed += Input.GetAxis("Horizontal") * acceleration.x;
	}
}