﻿using UnityEngine;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {
	public List<LevelPiece> possiblePieces;
	public int buffer = 0;

	Vector3 lastPiecePos = Vector3.zero;
	float lastCamPos = 0;
	float lastPieceDim;

	void Start() {
		for (int i = 0; i < buffer; i++) {
			spawnPiece();
		}
	}

	void Update() {
		if (Camera.main.transform.position.x >= lastCamPos + lastPieceDim) {
			spawnPiece();
		}
	}

	void spawnPiece() {
		var piece = possiblePieces[Random.Range(0, possiblePieces.Count)];
		lastPiecePos.x += piece.xDim;
		Instantiate(piece.gameObject, lastPiecePos, Quaternion.identity);
		lastPieceDim = piece.xDim;
		lastCamPos = Camera.main.transform.position.x; 
	}
}