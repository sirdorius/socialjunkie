﻿using UnityEngine;
using UnityEngine.UI;

public class InfiniteNews : MonoBehaviour {
	public GameObject newsItem;

	public void scrollHandler(Vector2 val) {
		if (val.y <= 0.2f) {
			newItem();
		}
	}

	private void newItem() {
		var item = Instantiate(newsItem) as GameObject;
		item.transform.SetParent(transform.GetChild(0), false);
	}

}