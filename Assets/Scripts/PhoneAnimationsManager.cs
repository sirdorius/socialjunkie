﻿using UnityEngine;
using GameEvents;
using SD;

public class PhoneAnimationsManager : MonoBehaviour {
	void OnEnable() {
		Events<PhoneIn>.RegisterHandler(phoneInHandler);
		Events<PhoneOut>.RegisterHandler(phoneOutHandler);
	}
	// TODO unregister

	public void phoneInHandler(GameEvents.PhoneIn e) {
		GetComponent<Animator>().SetTrigger("PhoneIn");
	}

	public void phoneOutHandler(GameEvents.PhoneOut e) {
		GetComponent<Animator>().SetTrigger("PhoneOut");
	}
}