﻿using UnityEngine;
using GameEvents;
using SD;

public class ControlsManager : MonoBehaviour {
	bool phoneVisible = false;
	void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			if (phoneVisible) {
				Events<PhoneOut>.Raise(new PhoneOut() { });
				phoneVisible = false;
			}
			else {
				Events<PhoneIn>.Raise(new PhoneIn() { });
				phoneVisible = true;
			}
		}
	}
}