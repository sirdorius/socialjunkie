﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Spawns GOs in random positions in a rectangular area around the GO it is placed on
/// </summary>

namespace SD
{
	public class SpawnInArea : MonoBehaviour
	{
		public GameObject spawnedGO;

		///< GameObject to instantiate
		public int number;

		public bool repeated;

		///< if true will spawn with a certain frequency
		public float repeatFrequency;

		///< frequency to respawn if repeated is true

		private Vector3 baseSpawnPosition;

		// Use this for initialization
		private void Start()
		{
			GetComponent<Renderer>().enabled = false;
			baseSpawnPosition = transform.position -
			                    new Vector3(transform.localScale.x/2f, transform.localScale.y/2f, transform.localScale.z/2f);
			spawn();
			if (repeated)
				StartCoroutine(spawnTask());
		}

		private void spawn()
		{
			for (int i = 0; i < number; i++)
			{
				var go = Instantiate(spawnedGO) as GameObject;
				go.transform.position = baseSpawnPosition +
				                        new Vector3(Random.Range(0, transform.localScale.x), Random.Range(0, transform.localScale.y),
					                        Random.Range(0, transform.localScale.z));
			}
		}

		private IEnumerator spawnTask()
		{
			yield return new WaitForSeconds(repeatFrequency);
			spawn();
			StartCoroutine(spawnTask());
		}
	}
}